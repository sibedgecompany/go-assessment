package v1

import (
	"context"

	"golang.org/x/text/language"

	"sibedge.com/go-assessment/pkg/models"
)

// API provides handlers API.
type API struct {
	Question
}

// Question provides API for working with questions.
type Question interface {
	Index(ctx context.Context, lang language.Tag) (models.Questions, error)
	New(ctx context.Context, question *models.Question) error
}
