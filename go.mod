module sibedge.com/go-assessment

go 1.13

require (
	cloud.google.com/go v0.56.0
	github.com/gammazero/workerpool v0.0.0-20200311205957-7b00833861c6
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.2
	github.com/go-playground/validator/v10 v10.2.0
	github.com/shurcooL/httpfs v0.0.0-20190707220628-8d4bc4ba7749 // indirect
	github.com/shurcooL/vfsgen v0.0.0-20181202132449-6a9ea43bcacd
	github.com/stretchr/testify v1.4.0
	go.uber.org/goleak v1.0.0
	golang.org/x/text v0.3.2
	google.golang.org/api v0.20.0
)
