package main

import (
	"context"
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"

	"cloud.google.com/go/translate"
	"github.com/gammazero/workerpool"
	"golang.org/x/text/language"
	"google.golang.org/api/option"
	v1 "sibedge.com/go-assessment/api/v1"
	"sibedge.com/go-assessment/internal/pkg/configs"
	fileRepo "sibedge.com/go-assessment/internal/pkg/repositories/file"
	"sibedge.com/go-assessment/internal/pkg/services"
	"sibedge.com/go-assessment/internal/pkg/transport/http"
	"sibedge.com/go-assessment/pkg/storage/file"
	"sibedge.com/go-assessment/pkg/translators"
)

// flags override env variables
var (
	serveAddr         = flag.String("serve.addr", ":9000", "serving address")
	serveReadTimeout  = flag.Int("serve.read.timeout", 10, "get timeout in seconds")
	serveWriteTimeout = flag.Int("serve.write.timeout", 20, "post timeout in seconds")

	storeMode = flag.String("store.mode", "json", "storage mode, any of json/csv")
	storeDir  = flag.String("store.dir", "./", "base directory for storing json/csv files")

	translateAPIKey   = flag.String("translate.api.key", "", "translate API key")
	translateTimeout  = flag.Int("translate.timeout", 10, "translate timeout in seconds")
	translateFailMode = flag.String("translate.fail.mode", "retry", "translate fail mode, any of retry/fast")
	translatePoolSize = flag.Int("translate.pool.size", 2, "translate worker pool size")
)

func main() {
	flag.Parse()
	cfg := configs.NewEnvConfig(
		serveAddr, serveReadTimeout, serveWriteTimeout,
		storeMode, storeDir,
		translateAPIKey, translateTimeout, translateFailMode, translatePoolSize)

	api, fn := buildApi(cfg)
	defer fn()

	errCh := make(chan error)
	server := http.NewServer(cfg.ServeAddr, cfg.ServeReadTimeout, cfg.ServeWriteTimeout, http.NewEngine(api))
	defer server.Shutdown(context.TODO()) //nolint:errcheck

	go func() {
		if err := server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			errCh <- err
		}
	}()

	quitCh := make(chan os.Signal, 1)
	signal.Notify(quitCh, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	select {
	case <-quitCh:
	case err := <-errCh:
		log.Fatal(err)
	}
}

func buildApi(cfg *configs.EnvConfig) (*v1.API, func()) {
	fs, err := file.NewLocalStorage(cfg.StoreMode, cfg.StoreDir)
	if err != nil {
		log.Fatal(err)
	}

	qRepo := fileRepo.NewQuestionRepository(fs)

	tc, err := translate.NewClient(context.TODO(), option.WithAPIKey(cfg.TranslateAPIKey))
	if err != nil {
		log.Fatal(err)
	}

	wp := workerpool.New(cfg.TranslatePoolSize)

	translator := translators.NewGoogleTranslator(tc, &translate.Options{
		Source: language.Und,
		Format: translate.Text,
	}, cfg.TranslateTimeout, cfg.TranslateFailMode, wp)

	return &v1.API{
			Question: services.NewQuestionService(qRepo, translator),
		}, func() {
			wp.StopWait()
			log.Print(tc.Close())
			log.Print(qRepo.Sync())
		}
}
