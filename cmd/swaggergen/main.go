package main

import (
	"log"
	"net/http"

	"github.com/shurcooL/vfsgen"
)

func main() {
	var fs http.FileSystem = http.Dir("../api/v1/assets/swagger")
	err := vfsgen.Generate(fs, vfsgen.Options{
		Filename:     "../api/v1/assets/swagger.go",
		PackageName:  "assets",
		VariableName: "Swagger",
	})
	if err != nil {
		log.Fatal(err)
	}
}
