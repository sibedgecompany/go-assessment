package test

import (
	"context"

	v1 "sibedge.com/go-assessment/api/v1"
	"sibedge.com/go-assessment/internal/pkg/transport/http"
)

func startTestServer(ctx context.Context, addr string, timeout int, api *v1.API) {
	server := http.NewServer(addr, timeout, timeout, http.NewEngine(api))
	defer server.Shutdown(context.TODO()) //nolint:errcheck

	go server.ListenAndServe() //nolint:errcheck

	<-ctx.Done()
}
