package mocks

import (
	"context"

	"github.com/stretchr/testify/mock"
	"golang.org/x/text/language"
)

type TranslatorMock struct {
	mock.Mock
}

func (m *TranslatorMock) Translate(_ context.Context, inputs []string, target language.Tag) ([]string, error) {
	args := m.Called(inputs, target)
	return args.Get(0).([]string), nil
}
