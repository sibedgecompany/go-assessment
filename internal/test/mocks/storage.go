package mocks

import (
	"encoding/json"

	"github.com/stretchr/testify/mock"
)

type StorageMock struct {
	mock.Mock
}

func (m *StorageMock) Store(_ string, _ interface{}) error {
	return nil
}

func (m *StorageMock) Load(_ string, v interface{}) error {
	args := m.Called()
	return json.Unmarshal([]byte(args.Get(0).(string)), v)
}
