package test

import (
	"context"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"
	"time"

	"github.com/stretchr/testify/mock"

	"github.com/gin-gonic/gin/binding"
	"github.com/stretchr/testify/assert"
	"go.uber.org/goleak"
	"golang.org/x/text/language"
	v1 "sibedge.com/go-assessment/api/v1"
	fileRepo "sibedge.com/go-assessment/internal/pkg/repositories/file"
	"sibedge.com/go-assessment/internal/pkg/services"
	"sibedge.com/go-assessment/internal/test/mocks"
)

var (
	postInputJson  = `{"text":"text2","createdAt":"2020-04-03T00:00:00.000Z","choices":[{"text":"choice1"},{"text":"choice2"},{"text":"choice3"}]}`
	postOutputJson = `{"text":"text2","createdAt":"2020-04-03 00:00:00","choices":[{"text":"choice1"},{"text":"choice2"},{"text":"choice3"}]}`
	inputJson      = `[{"text":"text","createdAt":"2020-04-03 00:00:00","choices":[{"text":"choice1"},{"text":"choice2"},{"text":"choice3"}]}]`
	outputJson     = `{"data":[{"text":"loc_text","createdAt":"2020-04-03 00:00:00","choices":[{"text":"loc_choice1"},{"text":"loc_choice2"},{"text":"loc_choice3"}]}]}`
	inputs         = []string{"text", "choice1", "choice2", "choice3"}
	outputs        = []string{"loc_text", "loc_choice1", "loc_choice2", "loc_choice3"}
	invalidOutputs = []string{"loc_text"}
)

func TestQuestion(t *testing.T) {
	defer goleak.VerifyNone(t)

	// Setup

	api := v1.API{}

	ctx, cancel := context.WithCancel(context.Background())
	go startTestServer(ctx, ":9900", 1, &api)
	time.Sleep(time.Second)

	// Run

	t.Run("QuestionIndex", func(t *testing.T) {
		testQuestionIndex(&api, t)
	})
	t.Run("QuestionIndexInvalid", func(t *testing.T) {
		testQuestionIndexInvalid(&api, t)
	})
	t.Run("QuestionNew", func(t *testing.T) {
		testQuestionNew(&api, t)
	})

	// Tear-down

	cancel()
}

func testQuestionIndex(api *v1.API, t *testing.T) {

	// Arrange

	sm := mocks.StorageMock{}
	sm.On("Load").Return(inputJson)

	tm := mocks.TranslatorMock{}
	tm.On("Translate", inputs, mock.Anything).Return(outputs)

	repo := fileRepo.NewQuestionRepository(&sm)
	api.Question = services.NewQuestionService(repo, &tm)

	// Act

	resp, err := http.Get("http://localhost:9900/questions?lang=en")
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}

	// Assert

	assert.Equal(t, http.StatusOK, resp.StatusCode)
	assert.Equal(t, outputJson, string(b))

	sm.AssertNumberOfCalls(t, "Load", 1)
	tm.AssertCalled(t, "Translate", inputs, language.English)
	tm.AssertNumberOfCalls(t, "Translate", 1)
}

func testQuestionIndexInvalid(api *v1.API, t *testing.T) {

	// Arrange

	sm := mocks.StorageMock{}
	sm.On("Load").Return(inputJson)

	tm := mocks.TranslatorMock{}
	tm.On("Translate", inputs, mock.Anything).Return(invalidOutputs)

	repo := fileRepo.NewQuestionRepository(&sm)
	api.Question = services.NewQuestionService(repo, &tm)

	// Act

	resp, err := http.Get("http://localhost:9900/questions?lang=en")
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}

	// Assert

	assert.Equal(t, http.StatusInternalServerError, resp.StatusCode)
	assert.Empty(t, string(b))

	tm.AssertCalled(t, "Translate", inputs, language.English)
	tm.AssertNumberOfCalls(t, "Translate", 1)
}

func testQuestionNew(api *v1.API, t *testing.T) {

	// Arrange

	sm := mocks.StorageMock{}
	sm.On("Load").Return(inputJson)

	repo := fileRepo.NewQuestionRepository(&sm)
	api.Question = services.NewQuestionService(repo, nil)

	// Act

	resp, err := http.Post("http://localhost:9900/questions", binding.MIMEJSON, strings.NewReader(postInputJson))
	if err != nil {
		t.Fatal(err)
	}
	defer resp.Body.Close()

	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		t.Fatal(err)
	}

	// Assert

	assert.Equal(t, http.StatusOK, resp.StatusCode)
	assert.Equal(t, postOutputJson, string(b))

	sm.AssertNumberOfCalls(t, "Load", 1)
}
