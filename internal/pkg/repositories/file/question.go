package file

import (
	"fmt"
	"sync"

	"sibedge.com/go-assessment/pkg/models"
)

// Storage for working with files.
type Storage interface {
	Store(filename string, v interface{}) error
	Load(filename string, v interface{}) error
}

type questionRepository struct {
	fs       Storage
	filename string

	qq   models.Questions
	m    sync.RWMutex
	once sync.Once
}

func NewQuestionRepository(fs Storage) *questionRepository {
	return &questionRepository{
		fs:       fs,
		filename: "questions",
	}
}

func (r *questionRepository) Index() (models.Questions, error) {
	err := r.load()
	if err != nil {
		return nil, err
	}

	r.m.RLock()
	defer r.m.RUnlock()
	return r.qq, nil
}

func (r *questionRepository) New(question *models.Question) error {
	err := r.load()
	if err != nil {
		return err
	}

	r.m.Lock()
	defer r.m.Unlock()

	for _, q := range r.qq {
		if q.Text == question.Text {
			return fmt.Errorf("question already exists")
		}
	}

	r.qq = append(r.qq, question)
	return nil
}

// Sync flushes all in-memory changes to filesystem.
func (r *questionRepository) Sync() error {
	if r.qq == nil {
		return nil
	}

	return r.fs.Store(r.filename, &r.qq)
}

func (r *questionRepository) load() error {
	var err error
	r.once.Do(func() {
		err = r.fs.Load(r.filename, &r.qq)
	})
	return err
}
