package http

import (
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"github.com/go-playground/validator/v10"
	v1 "sibedge.com/go-assessment/api/v1"
	"sibedge.com/go-assessment/api/v1/assets"
	"sibedge.com/go-assessment/internal/pkg/transport/http/handlers"
	"sibedge.com/go-assessment/internal/pkg/transport/http/middleware"
	"sibedge.com/go-assessment/internal/pkg/transport/http/validators"
)

func NewEngine(api *v1.API) *gin.Engine {
	// custom validator
	if v, ok := binding.Validator.Engine().(*validator.Validate); ok {
		_ = v.RegisterValidation("lang", validators.Lang)
	}

	engine := gin.Default()
	engine.Use(middleware.NewCorsMiddleware())

	// no private group/auth middleware by the task
	publicGroup := engine.Group("/")
	publicGroup.StaticFS("/swagger", assets.Swagger)

	h := handlers.NewQuestionHandler(api)
	publicGroup.Group("/questions").
		GET("", h.Index).
		POST("", h.New)

	return engine
}
