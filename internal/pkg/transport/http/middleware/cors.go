package middleware

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

func NewCorsMiddleware() gin.HandlerFunc {
	cfg := cors.DefaultConfig()
	cfg.AllowOriginFunc = func(string) bool {
		return true
	}
	cfg.AddAllowHeaders("Accept", "Accept-Encoding")
	return cors.New(cfg)
}
