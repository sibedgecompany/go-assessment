package requests

import (
	"time"

	"sibedge.com/go-assessment/pkg/models"
)

type QuestionIndexRequest struct {
	Lang string `form:"lang" binding:"required,lang"`
}

type QuestionNewRequest struct {
	Text      string    `json:"text" binding:"required,min=2,max=1024"`
	CreatedAt time.Time `json:"createdAt" binding:"required" time_format:"2006-01-02T15:04:05"`
	Choices   []struct {
		Text string `json:"text" binding:"required,min=2,max=1024"`
	} `json:"choices" binding:"required,len=3,dive"`
}

func (r QuestionNewRequest) Bind(model *models.Question) {
	model.Text = r.Text
	model.CreatedAt = r.CreatedAt.Format("2006-01-02 15:04:05")
	for _, choice := range r.Choices {
		model.Choices = append(model.Choices, &models.QuestionText{
			Text: choice.Text,
		})
	}
}
