package validators

import (
	"github.com/go-playground/validator/v10"
	"golang.org/x/text/language"
)

func Lang(fl validator.FieldLevel) bool {
	_, err := language.Parse(fl.Field().String())
	return err == nil
}
