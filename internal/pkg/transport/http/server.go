package http

import (
	"net/http"
	"time"
)

var ErrServerClosed = http.ErrServerClosed

// NewServer returns new Http server with routing engine.
func NewServer(addr string, readTimeout, writeTimeout int, handler http.Handler) *http.Server {
	return &http.Server{
		Addr:         addr,
		ReadTimeout:  time.Duration(readTimeout) * time.Second,
		WriteTimeout: time.Duration(writeTimeout) * time.Second,
		Handler:      handler,
	}
}
