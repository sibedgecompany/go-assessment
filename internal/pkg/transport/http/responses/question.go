package responses

import (
	"sibedge.com/go-assessment/pkg/models"
)

type QuestionIndexResponse struct {
	Data models.Questions `json:"data"`
}
