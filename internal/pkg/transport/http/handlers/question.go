package handlers

import (
	"context"
	"net/http"

	"sibedge.com/go-assessment/pkg/models"

	"golang.org/x/text/language"

	"github.com/gin-gonic/gin"
	v1 "sibedge.com/go-assessment/api/v1"
	"sibedge.com/go-assessment/internal/pkg/transport/http/requests"
	"sibedge.com/go-assessment/internal/pkg/transport/http/responses"
)

type questionHandler struct {
	api *v1.API
}

func NewQuestionHandler(api *v1.API) *questionHandler {
	return &questionHandler{
		api: api,
	}
}

func (h *questionHandler) Index(c *gin.Context) {
	var request requests.QuestionIndexRequest
	err := c.ShouldBind(&request)
	if err != nil {
		_ = c.AbortWithError(http.StatusUnprocessableEntity, err)
		return
	}

	var response responses.QuestionIndexResponse
	lang, _ := language.Parse(request.Lang)
	response.Data, err = h.api.Question.Index(c.Request.Context(), lang)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, &response)
}

func (h *questionHandler) New(c *gin.Context) {
	var request requests.QuestionNewRequest
	err := c.ShouldBind(&request)
	if err != nil {
		_ = c.AbortWithError(http.StatusUnprocessableEntity, err)
		return
	}

	var response models.Question
	request.Bind(&response)
	err = h.api.Question.New(context.TODO(), &response)
	if err != nil {
		_ = c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	c.JSON(http.StatusOK, &response)
}
