package services

import (
	"context"
	"fmt"

	"golang.org/x/text/language"
	"sibedge.com/go-assessment/pkg/models"
)

// QuestionRepository provides repository for questions.
type QuestionRepository interface {
	Index() (models.Questions, error)
	New(*models.Question) error
}

// Translator endpoint.
type Translator interface {
	Translate(context.Context, []string, language.Tag) ([]string, error)
}

type questionService struct {
	repo       QuestionRepository
	translator Translator
}

func NewQuestionService(
	repo QuestionRepository,
	translator Translator) *questionService {
	return &questionService{
		repo:       repo,
		translator: translator,
	}
}

func (s *questionService) Index(ctx context.Context, lang language.Tag) (models.Questions, error) {
	qq, err := s.repo.Index()
	if err != nil {
		return nil, err
	}

	var loc models.Questions
	var inputs, outputs []string
	for _, q := range qq {
		if _, ok := q.LocText(lang); !ok {
			// if translation not exists
			inputs = inputs[:0]
			inputs = append(inputs, q.Text)
			for _, choice := range q.Choices {
				inputs = append(inputs, choice.Text)
			}

			outputs, err = s.translator.Translate(ctx, inputs, lang)
			if err != nil {
				return nil, err
			}

			if len(outputs) != len(inputs) {
				return nil, fmt.Errorf("invalid outputs length")
			}

			q.NewLocText(lang, outputs[0])
			outputs = outputs[1:]
			for i, output := range outputs {
				q.Choices[i].NewLocText(lang, output)
			}
		}

		// clone object with loc fields
		loc = append(loc, q.LocQuestion(lang))
	}

	return loc, nil
}

func (s *questionService) New(_ context.Context, question *models.Question) error {
	return s.repo.New(question)
}
