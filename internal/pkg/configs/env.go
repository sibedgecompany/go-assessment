package configs

import (
	"os"
	"strconv"
)

// EnvConfig contains all available env variables.
type EnvConfig struct {
	ServeAddr         string
	ServeReadTimeout  int
	ServeWriteTimeout int

	StoreMode string
	StoreDir  string

	TranslateAPIKey   string
	TranslateTimeout  int
	TranslateFailMode string
	TranslatePoolSize int
}

// New returns new service config according to the env and flags.
func NewEnvConfig(
	serveAddr *string, serveReadTimeout *int, serveWriteTimeout *int,
	storeMode *string, storeDir *string,
	translateAPIKey *string, translateTimeout *int, translateFailMode *string, translatePoolSize *int,
) *EnvConfig {
	return &EnvConfig{
		ServeAddr:         envString("SERVE_ADDR", serveAddr),
		ServeReadTimeout:  envInt("SERVE_READ_TIMEOUT", serveReadTimeout),
		ServeWriteTimeout: envInt("SERVE_WRITE_TIMEOUT", serveWriteTimeout),
		StoreMode:         envString("STORE_MODE", storeMode),
		StoreDir:          envString("STORE_DIR", storeDir),
		TranslateAPIKey:   envString("TRANSLATE_API_KEY", translateAPIKey),
		TranslateTimeout:  envInt("TRANSLATE_TIMEOUT", translateTimeout),
		TranslateFailMode: envString("TRANSLATE_FAIL_MODE", translateFailMode),
		TranslatePoolSize: envInt("TRANSLATE_POOL_SIZE", translatePoolSize),
	}
}

func envString(key string, overrideVal *string) string {
	if overrideVal != nil && *overrideVal != "" {
		return *overrideVal
	}

	value, _ := os.LookupEnv(key)
	return value
}

func envInt(name string, overrideVal *int) int {
	if overrideVal != nil && *overrideVal != 0 {
		return *overrideVal
	}

	valueStr := envString(name, nil)
	value, _ := strconv.Atoi(valueStr)
	return value
}
