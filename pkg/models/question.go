package models

import (
	"fmt"
	"sync"

	"golang.org/x/text/language"
)

type Question struct {
	QuestionText
	CreatedAt string          `json:"createdAt"`
	Choices   []*QuestionText `json:"choices"`
}

type QuestionText struct {
	Text string `json:"text"`

	translations map[language.Tag]string
	m            sync.Mutex
}

type Questions []*Question

// LocQuestion returns localized copy of the question.
func (q *Question) LocQuestion(lang language.Tag) *Question {
	lq := Question{}
	lq.Text, _ = q.LocText(lang)
	lq.CreatedAt = q.CreatedAt
	for _, choice := range q.Choices {
		c := QuestionText{}
		c.Text, _ = choice.LocText(lang)
		lq.Choices = append(lq.Choices, &c)
	}
	return &lq
}

// LocText returns localized text if exists.
func (qt *QuestionText) LocText(lang language.Tag) (string, bool) {
	qt.m.Lock()
	defer qt.m.Unlock()

	if qt.translations == nil {
		qt.translations = map[language.Tag]string{}
	}

	loc, ok := qt.translations[lang]
	return loc, ok
}

// NewLocText adds localized text.
func (qt *QuestionText) NewLocText(lang language.Tag, text string) {
	qt.m.Lock()
	defer qt.m.Unlock()

	if qt.translations == nil {
		qt.translations = map[language.Tag]string{}
	}

	qt.translations[lang] = text
}

// FromStringSlice provides CSV reading capabilities.
func (qq *Questions) FromStringSlice(inputs [][]string) error {
	for _, inputs := range inputs {
		if len(inputs) != 5 {
			return fmt.Errorf("invalid inputs length")
		}

		q := Question{}
		q.Text = inputs[0]
		q.CreatedAt = inputs[1]
		q.Choices = append(q.Choices, &QuestionText{Text: inputs[2]})
		q.Choices = append(q.Choices, &QuestionText{Text: inputs[3]})
		q.Choices = append(q.Choices, &QuestionText{Text: inputs[4]})
		*qq = append(*qq, &q)
	}
	return nil
}

// ToStringSlice provides CSV writing capabilities.
func (qq *Questions) ToStringSlice() [][]string {
	var outputs [][]string
	for _, q := range *qq {
		outputs = append(outputs, []string{
			q.Text,
			q.CreatedAt,
			q.Choices[0].Text,
			q.Choices[1].Text,
			q.Choices[2].Text,
		})
	}
	return outputs
}
