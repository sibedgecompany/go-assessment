package translators

import (
	"context"
	"time"

	"cloud.google.com/go/translate"
	"golang.org/x/text/language"
)

// WorkerPool provides fade-in queue for translate operations.
type WorkerPool interface {
	Submit(task func())
	SubmitWait(task func())
}

type TranslateFailMode string

const (
	FailFast  TranslateFailMode = "fast"  //returns error immediately
	FailRetry TranslateFailMode = "retry" //try again
)

type googleTranslator struct {
	client   *translate.Client
	options  *translate.Options
	timeout  time.Duration
	failMode TranslateFailMode
	pool     WorkerPool
}

func NewGoogleTranslator(
	client *translate.Client,
	options *translate.Options,
	timeout int,
	failMode string,
	pool WorkerPool) *googleTranslator {
	return &googleTranslator{
		client:   client,
		options:  options,
		timeout:  time.Duration(timeout) * time.Second,
		failMode: TranslateFailMode(failMode),
		pool:     pool,
	}
}

func (t *googleTranslator) Translate(ctx context.Context, inputs []string, target language.Tag) ([]string, error) {
	var err error
	var tt []translate.Translation
	var outputs []string

	t.pool.SubmitWait(func() {
		for i := 0; i <= 1; i++ {
			tCtx, cancel := context.WithTimeout(ctx, t.timeout)
			tt, err = t.client.Translate(tCtx, inputs, target, t.options)
			if err != nil {
				switch t.failMode {
				case FailRetry:
					cancel()
					continue
				}
			}
			cancel()
			return
		}
	})

	for _, t := range tt {
		outputs = append(outputs, t.Text)
	}
	return outputs, err
}
