package file

import (
	"bytes"
	"encoding/csv"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"path/filepath"
)

type CSVRecordReader interface {
	FromStringSlice([][]string) error
}

type CSVRecordWriter interface {
	ToStringSlice() [][]string
}

type storeMode string

const (
	modeJSON storeMode = "json" //store in json
	modeCSV  storeMode = "csv"  //store in csv
)

type localStorage struct {
	mode storeMode
	dir  string
}

func NewLocalStorage(mode, dir string) (*localStorage, error) {
	fi, err := os.Stat(dir)
	if err != nil {
		return nil, err
	}

	if !fi.IsDir() {
		return nil, fmt.Errorf("%s is not a directory", dir)
	}

	switch storeMode(mode) {
	case modeJSON:
	case modeCSV:
	default:
		return nil, fmt.Errorf("not implemented %s store mode", mode)
	}

	return &localStorage{
		mode: storeMode(mode),
		dir:  dir,
	}, nil
}

func (s *localStorage) Store(filename string, v interface{}) error {
	var data []byte
	var err error

	switch s.mode {
	case modeJSON:
		data, err = json.MarshalIndent(v, " ", "  ")
		if err != nil {
			return err
		}
	case modeCSV:
		rw, ok := v.(CSVRecordWriter)
		if !ok {
			return fmt.Errorf("csv writing is not supported for %v", v)
		}

		var b bytes.Buffer
		writer := csv.NewWriter(&b)
		err = writer.WriteAll(rw.ToStringSlice())
		if err != nil {
			return err
		}

		data = b.Bytes()
	}
	return ioutil.WriteFile(s.filepath(filename), data, 0644)
}

func (s *localStorage) Load(filename string, v interface{}) error {
	data, err := ioutil.ReadFile(s.filepath(filename))
	if err != nil {
		return err
	}

	switch s.mode {
	case modeJSON:
		return json.Unmarshal(data, v)
	case modeCSV:
		rr, ok := v.(CSVRecordReader)
		if !ok {
			return fmt.Errorf("csv reading is not supported for %v", v)
		}

		reader := csv.NewReader(bytes.NewReader(data))
		records, err := reader.ReadAll()
		if err != nil {
			return err
		}

		return rr.FromStringSlice(records)
	}
	return nil
}

func (s *localStorage) filepath(filename string) string {
	return filepath.Join(s.dir, fmt.Sprintf("%s.%s", filename, s.mode))
}
